# Описание

- Проект использует yandex cloud (https://cloud.yandex.ru/docs), 
- terraform (https://developer.hashicorp.com/terraform/docs),
- ansible (https://docs.ansible.com/ansible/latest/user_guide/index.html).
___
- Для начала работ обязательно ознакомьтесь и выполните шаги из документациии по быстрому старту (https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart),
- Устанаовить ansible (https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) 
___
## Настройка среды 

1. Для того что бы развернуть проект необходимо  склонировать репозиторий
`git clone https://gitlab.com/my1stk8s/finalwork.part1.git`
2. Далее перейти в  папку с  проектом и  указать свои credentials в папке Infra в  файле variables.tf:
`
* cloud id: индификатор яндекс облака
* folder id: индификатор каталога в яндекс облаке
* token: токен яндекс облака
* ssh: в проект необходимо сгенерировать пару ключей в папку .ssh  для дальнейшего доступа 
`
- Так же указать публичный созданный ключ в файле /infra/instance/meta.yml в строке ssh-authorized-keys:
___
## Создание хранилища для terraform стейтов
1. Для поднятия хранилища необходимо перейти в папку tfstate.ycs3bucket и так же указать credentials в  файле variables.tf
* cloud id: индификатор яндекс облака
* folder id: индификатор каталога в яндекс облаке
* token: токен яндекс облака
2. Создание данного хранилища осуществляется единожды из папки  tfstate.ycs3bucket командой:
`terraform init, terraform apply --auto-approve`
___
## Развертываение инфраструктуры
1. Для развертывания инфраструктуры перейдите в папку Infra
2. Далее по аналогии  инициализируем `terraform init` и для начала установки  `terraform apply --auto-approve`
3. После окончания развертывания инфраструктуры идет автоматическая установка всех зависимостей путем раскатки ansibe ролей
___
## Дерево проекта
- App - приложение написанное на golang
- Infra - описанная инфраструктура на основе terraform
- service - необходимые зависимости описанные в ansibe role для установки и запуска приложения 
- monitoring - система мониторинга описанная в docker-compose.yml
- tfstate.ycs3bucket - создание хранилища стейтов для terraform описанная в terraform
- docs - документация к использованию
___
## Схема проекта
![alt](https://gitlab.com/my1stk8s/finalwork.part1/-/raw/master/docs/diagramm.png)
