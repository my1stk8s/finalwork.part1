# подключение к провайдеру и  и отправка стейта в хранилище
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.81.0"
    }
  }
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "s3bucketacc"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    
    skip_region_validation      = true
    skip_credentials_validation = true
  } 
}
# токены
provider "yandex" {
  token     = var.YC_TOKEN
  cloud_id  = var.YC_CLOUD_ID
  folder_id = var.YC_FOLDER_ID
}
# создание сети
resource "yandex_vpc_network" "network-tf" {
  name = "network-tf"
}

resource "yandex_vpc_subnet" "subnet-tf-2" {
  name           = "subnet-tf-2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-tf.id
  v4_cidr_blocks = ["192.168.7.0/24"]
}
# поднятие  инстансов по  шаблону
module "instance_1" {
  source = "./instance"
  name   = "gitlab"
  custom_subnet_id = yandex_vpc_subnet.subnet-tf-2.id
  instance_family_image = "ubuntu-2004-lts"
  zone_id = "ru-central1-b"
}

module "instance_2" {
  source = "./instance"
  name   = "prod"
  custom_subnet_id = yandex_vpc_subnet.subnet-tf-2.id
  instance_family_image = "ubuntu-2004-lts"
  zone_id = "ru-central1-b"
}
module "instance_3" {
  source = "./instance"
  name   = "dev"
  custom_subnet_id = yandex_vpc_subnet.subnet-tf-2.id
  instance_family_image = "ubuntu-2004-lts"
  zone_id = "ru-central1-b"
}

# Определение таргетов в группу load-balancer
resource "yandex_lb_target_group" "tf-target-group" {
  name = "tf-target-group"
  region_id = "ru-central1"

  target {
    subnet_id = yandex_vpc_subnet.subnet-tf-2.id
    address   = module.instance_2.internal_ip_address_vm
  }
}
# создание и определение параметров network_load_balancer
resource "yandex_lb_network_load_balancer" "tf-balancer" {
  name = "tf-balancer"

  listener {
    name = "tf-listener"
    port = 80
    target_port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.tf-target-group.id
    healthcheck {
      name = "tf-healthcheck"
        http_options {
          port = 80
          path = "/"
        }
    }
  }
}


resource "local_file" "inventory" {
  filename = "../service/hosts"
  content = <<EOF
[gitlab]
gitlab ansible_host=${module.instance_1.external_ip_address_vm} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}
[prod]
prod ansible_host=${module.instance_2.external_ip_address_vm} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}
[dev]
dev ansible_host=${module.instance_3.external_ip_address_vm} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}

  EOF
depends_on = [
    module.instance_3
  ]
}

# resource "null_resource" "ConfigureAnsibleLabelVariable" {
#   provisioner "local-exec" {
#     command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook  -i ../service/hosts ../service/install_docker.yml"
#   }
#   depends_on = [
#     local_file.inventory
#   ]
# }

