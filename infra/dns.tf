# создание зоны DNS и определение имен доменов высшего уровня 
resource "yandex_dns_zone" "moskvitin" {
  name        = "moskvitin-zone-name"
  description = "dns"

  labels = {
    label1 = "test-public"
  }

  zone    = "${var.dns_record}"
  public  = true
  private_networks = [yandex_vpc_network.network-tf.id]
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [(yandex_lb_network_load_balancer.tf-balancer.listener[*].external_address_spec[*].address)[0][0]]
}
resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "prod.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_2.external_ip_address_vm]
}

resource "yandex_dns_recordset" "rs3" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "dev.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_3.external_ip_address_vm]
}

resource "yandex_dns_recordset" "rs4" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "monitor.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_1.external_ip_address_vm]
}

resource "yandex_dns_recordset" "rs5" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "grafana.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_1.external_ip_address_vm]
}


resource "yandex_dns_recordset" "rs6" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "gitlab.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_1.external_ip_address_vm]
}
