
variable "YC_FOLDER_ID" {
  description = "Default folder id"
  
}
variable "YC_CLOUD_ID" {
  description = "Default cloud id"
}
variable "YC_TOKEN" {
  description = "Token"
}

variable "dns_record" {
  description = "dns"
  type        = string
  default     = "moskvitin.website."
}

variable "ssh_key_private" {
  description = "ssh"
  type        = string
  default     = "./.ssh/toster2"
}
