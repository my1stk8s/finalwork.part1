
resource "local_file" "inventory" {
  filename = "credentials"
  content = <<EOF

[default]
aws_access_key_id = "${yandex_iam_service_account_static_access_key.sa-static-key.access_key}"
aws_secret_access_key = "${yandex_iam_service_account_static_access_key.sa-static-key.secret_key}"

  EOF
 provisioner "local-exec" {
    
  command = "mv credentials ~/.aws/"
  }

}





